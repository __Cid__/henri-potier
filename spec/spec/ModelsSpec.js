describe("Models", function() {
	
	beforeEach(module('HenryPotier'));
	
	var $model;
	var $models;

	var hp_ws_retNOne;
	var hp_ws_retNTwo;
	
	beforeEach(inject(function() {
		// initialisation beforeAll Test ! ! 
		// xebia web service returned from 29/05/2016
		hp_ws_retNOne = {
				isbn: "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
				title: "Henri Potier à l'école des sorciers",
				price: 35,
				cover: "http://henri-potier.xebia.fr/hp0.jpg"
		};
		hp_ws_retNTwo = hp_ws_retNTwo = {
				isbn: "a460afed-e5e7-4e39-a39d-c885c05db861",
				title: "Henri Potier et la Chambre des secrets",
				price: 30,
				cover: "http://henri-potier.xebia.fr/hp1.jpg"
		};
	}));

	describe('Books', function () {
		
		beforeEach(inject(function(_Book_){
			// The injector unwraps the underscores (_) from around the parameter names when matching
			$model = _Book_;
		}));

		it("creation", function() {
			
			var invalidBookInit = new $model();
			var book = new $model(hp_ws_retNOne);

			expect(invalidBookInit).toBeDefined();
		    expect(invalidBookInit.getId).toBeUndefined();
		    
		    expect(book).toBeDefined();
		    expect(book.getId()).toEqual("c8fabf68-8374-48fe-a7ea-a00ccd07afff");
		});
		
		it("isSerializable", function () {
			var book = new $model(hp_ws_retNOne);
			
			expect(book.toJSON()).toEqual({
			    isbn: "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
			    title: "Henri Potier à l'école des sorciers",
			    price: 35,
			    cover: "http://henri-potier.xebia.fr/hp0.jpg",
			    qty: 0
			});
		});
		
	});
	
	describe('Orders', function () {
		
		var order;
		var book;
		
		beforeEach(inject(function(_Order_, _Book_){
			// The injector unwraps the underscores (_) from around the parameter names when matching
			$model = _Order_;
			$models = { Book: _Book_ };
			
			order = new $model();
			book = new $models.Book(hp_ws_retNOne);
			
			book.qty = 1;
			order.addBook(book);
			
		}));

		it("addBook", function() {
			
			expect(order.getBooksOrdered().length).toBe(1);
			expect(order.getBooksOrdered()[0].getId()).toContain(book.getId());
			
			book.qty = 2;
			expect(order.getBooksOrdered()[0].qty).toBe(1);
			
		});
		
		it("updateBook", function () {
						
			book.qty = 2;
			expect(order.getBooksOrdered()[0].qty).toBe(1);
			order.updateBook(book);
			expect(order.getBooksOrdered()[0].qty).toBe(2);
			
			book.qty = -3;
			order.updateBook(book);
			expect(order.getBooksOrdered().length).toBe(0);
			
		});
		
		it("getBooksId", function () {
			expect(order.getBooksId().length).toBe(1);
			expect(order.getBooksId()[0]).toBe("c8fabf68-8374-48fe-a7ea-a00ccd07afff");
			
			book.qty = 2;
			order.updateBook(book);
			expect(order.getBooksId().length).toBe(2);
			expect(order.getBooksId()).toEqual(["c8fabf68-8374-48fe-a7ea-a00ccd07afff", "c8fabf68-8374-48fe-a7ea-a00ccd07afff"]);
			
		});
		
		it("getTotal", function () {
			expect(order.getTotal()).toBe(35);
			
			book = new $models.Book(hp_ws_retNTwo);
			book.qty = 1;
			order.addBook(book);
			expect(order.getTotal()).toBe(65);
			
			book.qty = 3;
			order.updateBook(book);
			expect(order.getTotal()).toBe(125);
		});
		
		it("isSerializable", function () {
			
			expect(order.toJSON().books).toBeDefined();
			expect(order.toJSON().books.length).toBe(1);
			expect(order.toJSON().books[0].getId()).toBe("c8fabf68-8374-48fe-a7ea-a00ccd07afff");
			
		});
		
	});
	
	describe('Offer', function () {
		
		var order;
		var bookOne;
		var bookTwo;
		var offer;
		
		var hp_ws_ret_offer_book1;
		var hp_ws_ret_offer_book1and2;
		
		beforeEach(inject(function(_Offer_, _Order_, _Book_) {
			// xebia web service returned from 29/05/2016
			hp_ws_ret_offer_book1 = {
					offers: [{
						type: "percentage",
						value: 4
					}]
			};
			hp_ws_ret_offer_book1and2 = {
					offers: [{
								type: "percentage",
								value: 4
				             }, {
			            	 	type: "minus",
			            	 	value: 15
				             }, {
				            	 type: "slice",
				            	 sliceValue: 100,
				            	 value: 12
				             }]
			};
			
			// The injector unwraps the underscores (_) from around the parameter names when matching
			$model = _Offer_;
			$models = { Order: _Order_, Book: _Book_ };
			
			bookOne = new $models.Book(hp_ws_retNOne);
			bookOne.qty = 1;
			bookTwo = new $models.Book(hp_ws_retNTwo);
			bookTwo.qty = 1;
			
			order = new $models.Order();
			order.addBook(bookOne);
			order.addBook(bookTwo);
			
			offer = new $model(order);
			offer.refresh(hp_ws_ret_offer_book1and2);
		}));
		
		it("refresh", function () {
			
			expect(offer.getBill).toBeDefined();
			expect(offer.getBill()).toEqual({
				type: 2,
				value: 15,
				total: 50
			});
			
			bookTwo.qty = 0;
			order.updateBook(bookTwo);
			offer.refresh(hp_ws_ret_offer_book1);
			expect(offer.getBill()).toEqual({
				type: 1,
				value: 4,
				total: 35 * 0.96
			})
		});

		it("isSerializable", function () {
			
			expect(offer.toJSON().offers).toBeDefined();
			expect(offer.toJSON().offers.length).toBe(3);
			expect(offer.toJSON().offers[0].value).toBe(4);
			expect(offer.toJSON().offers[1].value).toBe(15);
			expect(offer.toJSON().offers[2].value).toBe(12);
			
		});
			
	});
	
});
