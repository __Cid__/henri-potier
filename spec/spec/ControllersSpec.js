describe("Controllers", function() {
	
	beforeEach(module('HenryPotier'));

	describe('booksCtrl', function () {});
	
	describe('mainCtrl', function () {});
	
	describe('orderCtrl', function () {
		
		var controller;
		var OrderManager;
		beforeEach(inject(function(_$controller_, _OrderManager_){
			// The injector unwraps the underscores (_) from around the parameter names when matching
			OrderManager = _OrderManager_;
			controller = _$controller_;
		}));
		
		it("pointing books", function () {
			var scope = {};
			var booksCtrl = controller('ordersCtrl', { $scope: scope, OrderManager: OrderManager })
			var books = OrderManager.getOrder().getBooksOrdered();
			expect(scope.books).toBe(books);
		});
			
	});
	
});
