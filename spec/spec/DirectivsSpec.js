describe("Directives", function() {
	
	beforeEach(module('HenryPotier'));

	describe('hpBill', function () {var $compile;
	    var $rootScope;
	
	    beforeEach(inject(function(_$compile_, _$rootScope_){
	    // The injector unwraps the underscores (_) from around the parameter names when matching
		    $compile = _$compile_;
		    $rootScope = _$rootScope_;
	    }));
	
	    it('testF', inject(function ($httpBackend, _$http_) {

	    	$httpBackend.whenGET("http://henri-potier.xebia.fr/books/").respond([]);
	    	
	    	$httpBackend.whenGET("directive/hpBill/hpBill.html").respond(function () {
	    			_$http_.get("directive/hpBill/hpBill.html")
	    			.then(function (html) {
	    				// Check that the compiled element contains the templated content
	    			    expect(element.html()).toContain("iouiytrds");
	    			});
	    			$rootScope.$apply();
	    	});
	    	
	    	var element = $compile("<hp-bill></hp-bill>")($rootScope);
	    	
	    	$rootScope.$apply();
	    }));
	});
	
	describe('hpOrder', function () {});
});
