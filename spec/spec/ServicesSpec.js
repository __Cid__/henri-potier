describe("Services", function() {
	
	beforeEach(module('HenryPotier'));
	
	beforeEach(inject(function ($httpBackend) {
	
		$httpBackend.whenGET("http://henri-potier.xebia.fr/books/").respond([]);
	
	}));

	describe('OrderManager', function () {
		
		var service;
		var models;
		var LOCAL_STORAGE_KEY;
		
		var books = {
			first:  {
				isbn: "c8fabf68-8374-48fe-a7ea-a00ccd07afff",
				title: "Henri Potier à l'école des sorciers",
				price: 35,
				cover: "http://henri-potier.xebia.fr/hp0.jpg"
			}, second: {
				isbn: "a460afed-e5e7-4e39-a39d-c885c05db861",
				title: "Henri Potier et la Chambre des secrets",
				price: 30,
				cover: "http://henri-potier.xebia.fr/hp1.jpg"
			}
		};
		
		var rootScope;
		
		beforeEach(inject(function(_OrderManager_, _Book_, _HP_OM_LS_NAME_, _$rootScope_){
			// The injector unwraps the underscores (_) from around the parameter names when matching
			LOCAL_STORAGE_KEY = _HP_OM_LS_NAME_;
			service = _OrderManager_;
			models = {Book: _Book_};
			
			rootScope = _$rootScope_;
			
			localStorage.clear();
			service.clearOrder();
			localStorage.setItem(LOCAL_STORAGE_KEY, "{\"order\":{\"books\":[{\"isbn\":\"c8fabf68-8374-48fe-a7ea-a00ccd07afff\",\"title\":\"Henri Potier à l'école des sorciers\",\"price\":35,\"cover\":\"http://henri-potier.xebia.fr/hp0.jpg\",\"qty\":1},{\"isbn\":\"a460afed-e5e7-4e39-a39d-c885c05db861\",\"title\":\"Henri Potier et la Chambre des secrets\",\"price\":30,\"cover\":\"http://henri-potier.xebia.fr/hp1.jpg\",\"qty\":1}]},\"offer\":{\"offers\":[{\"type\":\"percentage\",\"value\":4},{\"type\":\"minus\",\"value\":15},{\"type\":\"slice\",\"sliceValue\":100,\"value\":12}]}}");
			service.refresh();
			
			expect(service.getOrder().getBooksOrdered().length).toBe(2);
			expect(service.getOrder().getTotal()).toBe(65);

		}));
		
		it('refresh', function () {
			localStorage.clear();
			service.refresh();
			expect(service.getOrder().getBooksOrdered().length).toBe(0);
			expect(service.getOrder().getTotal()).toBe(0);
			
		});
		
		it('addInOrder add -', function () {
			expect(service.getOrder().getBooksOrdered().length).toBe(2);
			expect(service.getOrder().getTotal()).toBe(65);
			
			expect(service.getOffer().getBill().type).toBe(2);
			expect(service.getOffer().getBill().value).toBe(15);
			expect(service.getOffer().getBill().total).toBe(50);				
		});
		
		it('addInOrder add add', inject(function ($httpBackend) {
			// needed to make one call to /commercialOffers
			service.getOrder().getBooksOrdered()[0].qty = 3;
			expect(service.getOrder().getBooksOrdered().length).toBe(2);
			expect(service.getOrder().getTotal()).toBe(135);
			
			bookTwo = new models.Book(books.second);
			bookTwo.qty = 1;
			service.addInOrder(bookTwo);
			expect(service.getOrder().getTotal()).toBe(165);
			
			expect(service.getOrder().getBooksId().length).toBe(5);
			
			$httpBackend.whenGET("http://henri-potier.xebia.fr/books/c8fabf68-8374-48fe-a7ea-a00ccd07afff,c8fabf68-8374-48fe-a7ea-a00ccd07afff,c8fabf68-8374-48fe-a7ea-a00ccd07afff,a460afed-e5e7-4e39-a39d-c885c05db861,a460afed-e5e7-4e39-a39d-c885c05db861/commercialOffers").respond("{\"offers\": [{\"type\": \"percentage\",\"value\": 10},{\"type\": \"minus\",\"value\": 30},{\"type\": \"slice\",\"sliceValue\": 80,\"value\": 14}]}");
			$httpBackend.flush();
			rootScope.$apply();

			expect(service.getOffer().getBill().type).toBe(2);
			expect(service.getOffer().getBill().value).toBe(30);
			expect(service.getOffer().getBill().total).toBe(135);				
			
		}));
		
		it('addInOrder add replace', inject(function ($httpBackend) {
			service.getOrder().getBooksOrdered()[0].qty = 2;
			
			expect(service.getOrder().getBooksOrdered().length).toBe(2);
			expect(service.getOrder().getTotal()).toBe(100);
			
			bookTwo = new models.Book(books.second);
			bookTwo.qty = 6;
			service.addInOrder(bookTwo, false);
			expect(service.getOrder().getTotal()).toBe(250);
			
			expect(service.getOrder().getBooksId().length).toBe(8);
			
			$httpBackend.whenGET("http://henri-potier.xebia.fr/books/c8fabf68-8374-48fe-a7ea-a00ccd07afff,c8fabf68-8374-48fe-a7ea-a00ccd07afff,a460afed-e5e7-4e39-a39d-c885c05db861,a460afed-e5e7-4e39-a39d-c885c05db861,a460afed-e5e7-4e39-a39d-c885c05db861,a460afed-e5e7-4e39-a39d-c885c05db861,a460afed-e5e7-4e39-a39d-c885c05db861,a460afed-e5e7-4e39-a39d-c885c05db861/commercialOffers").respond("{\"offers\": [{\"type\": \"percentage\",\"value\": 10},{\"type\": \"minus\",\"value\": 30},{\"type\": \"slice\",\"sliceValue\": 80,\"value\": 14}]}");
			$httpBackend.flush();
			
			expect(service.getOffer().getBill().type).toBe(3);
			expect(service.getOffer().getBill().value).toBe(14);
			expect(service.getOffer().getBill().slice).toBe(80);
			expect(service.getOffer().getBill().total).toBe(208);				
			
		}));
		
		it('addInOrder add minus', inject(function ($httpBackend) {
			bookTwo = new models.Book(books.second);
			bookTwo.qty = -4;
			service.addInOrder(bookTwo);
			
			expect(service.getOrder().getTotal()).toBe(35);
			expect(service.getOrder().getBooksId().length).toBe(1);
			
			$httpBackend.whenGET("http://henri-potier.xebia.fr/books/c8fabf68-8374-48fe-a7ea-a00ccd07afff/commercialOffers").respond("{\"offers\": [{\"type\": \"percentage\",\"value\": 5}]}");
			$httpBackend.flush();
			
			expect(service.getOffer().getBill().type).toBe(1);
			expect(service.getOffer().getBill().value).toBe(5);
			expect(service.getOffer().getBill().total).toBe(33.25);
			
		}));
		
	});
	
	describe('BooksManager', function () {});
	
});
