angular.module('HenryPotier')
.factory('Offer', ['Order', function (Order) {
	/**
	 * 
	 * @param object wsret | Order object returned by xebian webservice or Order
	 */
	function Offer (order) {
		
		var wsreturn = null;
		
		var order = order;
		
		var percentage = 0;
		var minus = 0;
		var slice = {
				interval: -1,
				value: 0
		};
		
		var bill = null;
		
		function applySlice() {
			
			var i = order.getTotal() / slice.interval;
			var total = order.getTotal();
			
			while (i-- > 1) {
				total -= slice.value;
			}
			
			return total;
		}
		
		function getMin (array) {
			
			var min = {
				type: 0,
				total: order.getTotal()
			}
			
			for ( var i = 0; i < array.length; ++i ) {
				if (array[i].total < min.total) {
					min = array[i];
				}
			}
			
			return min;
			
		}
		
		function findBestDeduction () {
			
			var percent = percentage ? 1 - (percentage / 100) : 1; 
			bill = getMin ([{
                type: 1,
                value: percentage,
                total: order.getTotal() * percent
			}, {
				type: 2,
				value: minus,
				total: order.getTotal() - minus
			}, {
				type: 3,
				value: slice.value,
				slice: slice.interval,
				total: applySlice(),
			}]);
		};
		
		/**
		 * This function allow you to update the object without changing his pointer
		 * 
		 * @param object wsret object return from a webia webservice
		 */
		this.refresh = function (wsret) {
			if ( !wsret || !wsret.offers) {
				console.warn('invalid argument passed to refresh Offer');
				return null;
			}
			
			wsreturn = wsret;
			percentage = 0;
			minus = 0;
			slice = {
					interval: -1,
					value: 0
			};
			
			var bill = null;
			
			for (var i = 0; i < wsret.offers.length; ++i) {
				item = wsret.offers[i];
				switch (item.type) {
					case 'percentage':
						percentage = item.value;
						break;
					case 'minus':
						minus = item.value;
						break;
					case 'slice':
						slice = {
							interval: item.sliceValue,
							value: item.value
						};
						break;
					default:
						console.warn('property ' + prop + ' isn\'t known from offer "class"');
						break;
				}
			}
			
			findBestDeduction();
		}
		
		this.getBill = function () {
			return bill;
		}
		
		this.toJSON = function () {
			return wsreturn;
		}
		
		return this;
	}
	
	return Offer;
}]);