angular.module('HenryPotier')
.factory('Order', ['Book', function (Book) {
	/**
	 * Allow offers management with books
	 */
	function Order () {
		
		var books = [];
		
		this.addBook = function (book) {
			
			if (book instanceof Book) {

				var myBook = new Book ({
					isbn: book.getId(),
					title: book.getTitle(),
					cover: book.getCover(),
					price: book.getPrice()
				});
				myBook.qty = book.qty;
				
				if ( ! books[myBook.getId()]) {
					books.push(myBook);
					books[myBook.getId()] = myBook;
					myBook.indexOf = books.indexOf(myBook);
				}
			}
		}
		
		this.updateBook = function (book) {
			
			if (book.qty < 1 && books[book.getId()]) {
				
				var index = books[book.getId()].indexOf;
				books.splice(index, index + 1);
				delete books[book.getId()];
				
			} else {
				
				books[book.getId()].qty = book.qty;
				
			}
			
		}
			
		this.getBooksId = function () {
			var ret = [];
			for (var i = 0; i < books.length; ++i) {
				var qty = books[i].qty;
				while (qty > 0) {
					ret.push(books[i].getId());
					--qty;
				}
			}
			return ret;
		};
		
		this.getBooksOrdered = function () {
			return books;
		};
		
		this.getTotal = function () {
			var total = 0;
			
			books.forEach(book => {
				total += book.getPrice() * book.qty;
			});
			
			return total;
		};
		
		this.toJSON = function () {
			return {
				books: books
			}; 
		}
		
		return this;
	}	
	
	return Order;
}]);