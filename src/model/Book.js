angular.module('HenryPotier')
.factory('Book', [function () {
	/**
	 * 
	 * @param object wsret object returned by xebian webservice
	 */
	function Book (wsret) {
		
		if ( !wsret ) {
			console.warn('invalid argument passed to instanciate Book');
			return null;
		}
		
		var id = wsret.isbn;
		var title = wsret.title;
		var price = parseInt(wsret.price);
		var cover = wsret.cover;
		if (wsret.qty) {
			this.qty = parseInt(wsret.qty);
		} else {
			this.qty = 0;
		}
		
		this.getId = function () {
			return id;
		};
		
		this.getTitle = function () {
			return title;
		};
		
		this.getPrice = function () {
			return price;
		};
		
		this.getCover = function () {
			return cover;
		};
		
		this.toJSON = function () {
			return {
				isbn: id,
				title: title,
				price: price,
				cover: cover,
				qty: this.qty
			};
		}
		
		return this;
	}
	
	return Book;
}]);