angular.module('HenryPotier')
.controller('hpOrderCtrl', ['$scope', 'BooksManager', 'OrderManager', '$state',
function ($scope, BooksManager, OrderManager, $state) {
	BooksManager.refresh();
	$scope.books = BooksManager.getBooks();
	$scope.order = OrderManager.getOrder();
	$scope.offer = OrderManager.getOffer();
	
	$scope.total = 0;
	$scope.wait = true;
	
	$scope.updateTotal = function (book) {
		$scope.total = 0;
		$scope.wait = true;
		$scope.books.forEach(book => {
			if (book.qty > 0) {
				$scope.total += book.qty * book.getPrice();
				$scope.wait = false;
			}
		});
	};
	$scope.validate = function () {
		$scope.books.forEach(book => {
			if (book.qty > 0) {
				OrderManager.addInOrder(book);
			}
		});
		$state.go('orders');
	}
}]);