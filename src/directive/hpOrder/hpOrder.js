angular.module('HenryPotier')
.directive('hpOrder', ['OrderManager', function (OrderManager) {
	return {
		restrict: 'E',
		transclude: true,
		controller: 'hpOrderCtrl',
		templateUrl: 'directive/hpOrder/hpOrder.html'
	};
}])