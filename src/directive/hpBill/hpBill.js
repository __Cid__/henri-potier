angular.module('HenryPotier')
.directive('hpBill', ['OrderManager', function (OrderManager) {
	return {
		restrict: 'E',
		controller: 'hpBillCtrl',
		templateUrl: 'directive/hpBill/hpBill.html'
	};
}])