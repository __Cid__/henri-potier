angular.module('HenryPotier')
.controller('sendModalCtrl', ['$scope', '$uibModalInstance', 'books', 'sum',
function($scope,$uibModalInstance, books, sum) {
	
	$scope.books = books;
	$scope.sum = sum;
	
	$scope.ok = function () {
		$uibModalInstance.close();
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);