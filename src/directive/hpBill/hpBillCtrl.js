angular.module('HenryPotier')
.controller('hpBillCtrl', ['$scope', 'OrderManager', '$state', '$uibModal',
function ($scope, OrderManager, $state, $uibModal) {
	$scope.getBill = OrderManager.getOffer().getBill;
	
	$scope.updateOffer = function (book, oldValue) {
		OrderManager.addInOrder(book, false);
	};
	
	$scope.buy = function () {
		
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'directive/hpBill/modal/send.html',
			controller: 'sendModalCtrl',
			resolve: {
				books: function () {
					return OrderManager.getOrder().getBooksOrdered();
				},
				sum: $scope.getBill().total
			}
		});
	
		modalInstance.result.then(function () {
			OrderManager.clearOrder();
			$state.go('home');
		}, function () {
			// nothing to do
		}
	)};

	$scope.clear = function () {
		var modalInstance = $uibModal.open({
			animation: true,
			size: 'sm',
			template: '<div class="modal-header">'
					+ '<button class="btn btn-primary" type="button" ng-click="ok()">Vider le panier</button>'
					+ '<span style="margin-left: 4px;"></span>'
					+ '<button class="btn btn-warning" type="button" ng-click="cancel()">Garder le panier</button>'
				+ '</div>',
			controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
				$scope.ok = function () {
					$uibModalInstance.close();
				};

				$scope.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};
			}]
		});
	
		modalInstance.result.then(function () {
			OrderManager.clearOrder();
			$state.go($state.current, {}, {reload: true});
		}, function () {
			// nothing to do
		});
	}
}]);