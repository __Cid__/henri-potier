angular.module('HenryPotier')
// HenryPotier_BooksManager_LocalStorage_NAME
.constant('HP_BM_LS_NAME', 'HenryPotier_BooksManager_Books')
.service('BooksManager', ['HPHttp', 'Book', 'HP_BM_LS_NAME',
function (HPHttp, Book, HP_BM_LS_NAME) {
	
	var books = [];
	
	function setBooks(ret) {

		books = [];
		
		for (var i = 0; i < ret.length; ++i) {
			var book = new Book (ret[i]);
			books.push(book);
			books[book.getId()] = book;
		}
		
		localStorage.setItem(HP_BM_LS_NAME, JSON.stringify(ret));
		
	}
	
	return {
		/**
		 * refresh check if isset local data from books
		 * if yes ==> update
		 * else ==> nothing
		 */
		refresh: function () {
			HPHttp.getBooks(function (books) {
				setBooks(books);
			}, function (err) {
				if ( localStorage.getItem ( HP_BM_LS_NAME ) ) {
					setBooks (
							JSON.parse ( localStorage.getItem ( HP_BM_LS_NAME ) )
					);
				}
			});
		},
		/**
		 * index all books
		 * 
		 * @return array where books are indexed, you have [{}, ...] and {id: ...},{id: ...}
		 * available from the same array 
		 */
		getBooks: function () {
			return books;
		},
		getBook: function (id) {
			return books[id];
		}
	};
}]);