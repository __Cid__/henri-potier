angular.module('HenryPotier')
.service('HPHttp', ['$http', function ($http) {
	
	var addrs = {
			rootAddr : 'http://henri-potier.xebia.fr/books/',
			books: '',
			book: '/commercialOffers'
	};
	
	function call (method, url, successCallback, errorCallback) {
		$http({
			method: method,
			url: url
		}).then(function (response) {
			
			successCallback(response.data);
			
		}, function (response) {
			
			console.error('Call for books failed');
			console.warn(response);
			if (errorCallback) {
				errorCallback(response);
			}
			
		});
	}
	
	function getCall (url, callBack) {
		call ('GET', url, callBack);
	}
	
	return {
		/**
		 * will execute the callback function passed with the books returned
		 * 
		 * @params function callBack function called with books
		 * @return promise
		 */
		getBooks: function (successCallback, errorCallback) {
			return getCall(addrs.rootAddr + addrs.books, successCallback, errorCallback);
		},
		/**
		 * 
		 * will execute the callback function passed with the offer return
		 * 
		 * @params function callback function called with offer result
		 * @params array idList list of books's id
		 * @return promise
		 */
		order: function (idList, callback, errorCallback) {
			return getCall(addrs.rootAddr + idList + addrs.book, callback, errorCallback);
		}
	};
}]);