angular.module('HenryPotier')
// HenryPotier_OrderManager_LocalStorage_NAME
.constant('HP_OM_LS_NAME', 'HenryPotier_OrderManager_Offer_Order')
.service('OrderManager', ['BooksManager', 'Order', 'Offer', 'Book', 'HPHttp', 'HP_OM_LS_NAME',
function (BooksManager, Order, Offer, Book, HPHttp, HP_OM_LS_NAME) {
	// this order is shared with the offer via pointer ... !
	var order = new Order();
	var offer = new Offer(order);
	
	return {
		/**
		 * refresh check if isset local data from orders
		 * if yes ==> update
		 * else ==> nothing
		 */
		refresh: function () {
			
			localData = JSON.parse(localStorage.getItem(HP_OM_LS_NAME));
			if (localData) {
				
				var books = localData.order.books;
				for (var i = 0; i < books.length; ++i) {
					var book = new Book (books[i]);
					book.qty = parseInt(books[i].qty);
					if ( order.getBooksId().indexOf(book.getId()) === -1) {
						order.addBook ( book );
					}
				}
				
				offer.refresh (localData.offer);
			} else {
				order = new Order();
				offer = new Offer(order);
			}
			
		},
		/**
		 * Add a book to order
		 * offer will be update as a promise, you can't access refreshed offer after this call
		 * wait a JavaScript execution cycle
		 * 
		 * @params Book book book to manage
		 * @params Boolean isAdditionnable, add or not the existing quantity with a new one. Default : true
		 */
		addInOrder: function (book, isAdditionnable) {
			
			if (isAdditionnable !== false) {
				isAdditionnable = true;
			}
			
			// if the book is unknown
			if ( !order.getBooksOrdered()[book.getId()] ) {
				order.addBook(book);
			// if it is known and we want to add qty
			} else if (isAdditionnable) {
				if (book.qty > 0) {
					book.qty += order.getBooksOrdered()[book.getId()].qty;
				} else {
					book.qty = 0;
				}
				order.updateBook (book);
			// if it is know and we want to replace the qty
			} else {
				order.updateBook (book);
			}
			
			HPHttp.order(order.getBooksId(), function (ret) {
				
				if (ret === null) {
					offer.refresh({offers: []});
				} else {
					offer.refresh(ret);
				}
				
				localStorage.setItem(HP_OM_LS_NAME, JSON.stringify({
					order: order,
					offer: offer
				}));
				
			}, function (err) {
				if (order.getBooksId()) {
					console.error(err);
				}
			});
		},
		updateTotal: function (total) {
			
			total = 0;
			order.getBooksOrdered().forEach(book => {
				if (book.qty > 0 ) {
					total += (book.qty * book.getPrice());
				}
			});
			
			return total;
		},
		getOrder: function () {
			return order;
		},
		/**
		 * The order is update with promise,
		 * you need to wait resolving it to access it
		 */
		getOffer: function () {
			return offer;
		},
		clearOrder: function () {
			localStorage.removeItem(HP_OM_LS_NAME);
			BooksManager.refresh();
			this.refresh();
		}
	};
}]);