angular.module('HenryPotier')
.controller('ordersCtrl', ['$scope', 'OrderManager',
function ($scope, OrderManager) {
	$scope.books = OrderManager.getOrder().getBooksOrdered();
}]);