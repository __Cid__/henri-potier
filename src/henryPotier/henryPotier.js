angular.module('HenryPotier', ['ui.router', 'ui.bootstrap'])
.config(['$stateProvider', '$urlRouterProvider',
		function ($stateProvider, $urlRouterProvider) {

	$stateProvider
    	.state('home', {
			url: '/',
			templateUrl: 'views/home.html',
			controller: 'mainCtrl'
		}).state('books', {
			url: '/books',
			templateUrl: 'views/books.html',
			controller: 'booksCtrl'
		}).state('orders', {
			url: '/orders',
			templateUrl: 'views/orders.html',
			controller: 'ordersCtrl'
		}).state('order', {
			url: '/order/:list',
			controller: 'orderCtrl',
			resolve: {
				list: ['$stateParams', 'HPHttp', function($stateParams, HPHttp) {
					
					return $stateParams.list.split(',');
					
				}]
			}
		});
	
	$urlRouterProvider.otherwise("/");
  
}]).run(['BooksManager', 'OrderManager', function (BooksManager, OrderManager) {

	BooksManager.refresh();
	OrderManager.refresh();
	
}]);